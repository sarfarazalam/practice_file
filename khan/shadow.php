<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style>
.submit {
	border:1px solid #4589f7;
	background:#4589f7;
	font-size:13px;
	padding:7px 15px;
	color:#fff;
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	border-radius:3px;
}
.submit:active {
	box-shadow:inset 0 0 1px #fff, inset 0 0 1px #fff, inset 0 0 1px #fff, inset 0 0 1px #fff;
}
</style>
</head>

<body>
<input type="button" class="submit" value="Sign in">

</body>
</html>