<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Blue World</title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="screen" />
<link href="css/contact.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="wrap">
  <header>
    <div class="logo"><span>WWW.BLUEWORLD.COM</span>
      <div class="logoa">logoa</div>
    </div>
    <div class="form">
      <form class="subscribe" action="#" method="post">
        <ul>
          <li>
            <label for="login">Login</label>
            <input class="field" type="text" name="login" size="30" maxlength="50"  placeholder="Enter your user name" />
          </li>
          <li>
            <label for="login">Password</label>
            <input class="field" type="password" name="passw" size="30" maxlength="50" placeholder="Enter password" />
          </li>
          <li>
            <input type="submit" class="submitter" style="display:none;">
          </li>
        </ul>
      </form>
    </div>
    <div class="menu">
      <ul>
        <li><a href="index.php"><span>Home</span></a></li>
        <li class="selected"><a href="about.php"><span>About us</span></a></li>
        <li><a href="#"><span>Services</span></a></li>
        <li><a href="contactBluew.php"><span>Contact us</span></a></li>
      </ul>
    </div>
  </header>
  <div class="main-content">
    <!--main-content-->
    <h1><span style="border-bottom:2px solid #a0acba;">ABOUT</span>BLUEWORLD</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce malesuada augue quis neque porttitor quis placerat nisl commodo. In odio turpis, euismod et blandit vel, tristique eu erat. Donec gravida fringilla hendrerit. Duis bibendum turpis metus. Donec faucibus euismod dui, ac rhoncus ligula dictum eget. Ut sit amet massa et felis gravida gravida vel et leo. Maecenas mattis tortor ante. Vivamus venenatis eleifend neque, eget congue metus aliquam at. In hendrerit quam vitae tortor bibendum rhoncus ultricies metus hendrerit. Duis sagittis nibh purus. Donec dignissim libero sit amet leo vulputate convallis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque et magna tortor. Donec suscipit lacinia gravida. Aliquam dapibus porttitor aliquet.
    <p>Curabitur elementum ultricies justo ut vehicula. Nam ante odio, molestie ut fringilla vel, iaculis vel nisl. Mauris ornare malesuada augue, vitae vestibulum sapien porttitor in. Curabitur diam nulla, lacinia viverra mollis ac, tincidunt non tellus. Duis feugiat aliquet condimentum. Mauris accumsan urna ac lectus ultrices fringilla. Duis volutpat libero quis mi semper vel sagittis mauris ornare. Cras et risus nec nisi interdum ultrices. In sed elit arcu. Praesent a adipiscing ipsum. Curabitur id nibh lectus, sit amet viverra magna. Integer vitae felis justo. Ut congue, massa nec malesuada ornare, justo elit condimentum arcu, accumsan volutpat nisi enim eu mi.</p>
    <p>Donec eget sem nunc, at fringilla libero. Nulla pretium egestas purus sit amet aliquet. Etiam ullamcorper arcu sit amet orci eleifend laoreet. Pellentesque at lacus dui. Morbi iaculis libero dui, eu dictum lorem. Duis eu justo purus. Nullam non eros in est consequat suscipit eget non neque. Aenean vulputate lectus sit amet magna tempor ultrices. Vestibulum et quam a purus sodales lacinia. Phasellus suscipit ipsum vel nisl laoreet porta. Sed tempus dolor ante. Cras velit erat, blandit a tempor non, eleifend non turpis.</p>
    <p>Donec iaculis mauris vitae purus posuere pretium. Ut ac orci arcu. Cras lacus magna, volutpat pulvinar sagittis quis, rhoncus quis ante. Sed sit amet nisi at lectus sollicitudin pulvinar. Aliquam erat volutpat. Aenean diam libero, ultricies tincidunt lobortis nec, cursus sit amet odio. Sed eget augue in nisi volutpat tempor sit amet quis felis. Donec venenatis, magna id laoreet fermentum, neque nibh tristique magna, et aliquam ipsum ante vitae quam. Cras et est diam, a consectetur massa. Maecenas porta, turpis vel iaculis adipiscing, augue enim faucibus ante, eget posuere sapien mi in nisl. Nam sollicitudin lectus at turpis imperdiet malesuada. Aenean non neque non risus varius accumsan. Suspendisse potenti. Sed sit amet orci quis odio bibendum porttitor et euismod eros. Praesent condimentum, sapien sit amet commodo aliquam, neque quam bibendum lorem, sit amet cursus mi erat nec tellus. Vivamus tortor erat, commodo nec tempus vitae, lobortis quis odio. Donec imperdiet, turpis quis commodo laoreet, risus massa feugiat augue, quis mattis enim diam eget odio. </p>
    <div class="socbox"> <a href="http://www.facebook.com/md.sarfaraz.a" target="_blank"><img src="img/f.png"/></a><a href="http://www.twitter.com/mdsrazalam" target="_blank"><img src="img/t.png" /></a><a href="#"><img src="img/s.png" /></a><a href="#"><img src="img/fr.png" /></a></div>
  </div>
  <!--End main-content-->
  <footer>
    <div class="left">&copy;<a href="www.sarfarazalam.com">www.sarfarazalam.com</a></div>
  </footer>
</div>
<!--End wrap-->
</body>
</html>
