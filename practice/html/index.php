<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="Author" content="Hamid Raza">
<title>M R FEDGE And ASSOCIATES</title>
<link href="css/style.css" type="text/css" rel="stylesheet">
<script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/Myriad_Pro_400-Myriad_Pro_600.font.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
</head>

<body>
<div id="webbg"><div></div><img src="img/bg1.jpg" alt=""></div>
<div id="wrap">
	<div class="leftbar">
    	<div id="logo"><a href="#">M R FEDGE And ASSOCIATES</a></div>
        <div id="tagline">M R Fedge &nbsp; Associates</div>
        <div class="blackbar"></div>
        <div id="whatsnew">
        	<ul>
            	<!--<li><a href="#"><img src="img/thumb-1.jpg" alt=""></a></li>
            	<li>&nbsp;</li>
            	<li>&nbsp;</li>
            	<li><a href="#"><img src="img/thumb-2.jpg" alt=""></a></li>
            	<li><a href="#"><img src="img/thumb-3.jpg" alt=""></a></li>
            	<li>&nbsp;</li>
            	<li>&nbsp;</li>
            	<li><a href="#"><img src="img/thumb-4.jpg" alt=""></a></li>
            	<li><a href="#"><img src="img/thumb-5.jpg" alt=""></a></li>
            	<li>&nbsp;</li>-->
				<li class='new1 image'><a href="#"><img src="img/thumb-1.jpg" alt=""></a></li>
            	<li class='new1 caption'><div class="captiondiv">Interior<br>or<br>Architect</div></li>
            	<li class='new2 caption'><div class="captiondiv">Interior<br>or<br>Architect</div></li>
            	<li class='new2 image'><a href="#"><img src="img/thumb-2.jpg" alt=""></a></li>
            	<li class='new3 image'><a href="#"><img src="img/thumb-3.jpg" alt=""></a></li>
            	<li class='new3 caption'><div class="captiondiv">Interior<br>or<br>Architect</div></li>
            	<li class='new4 caption'><div class="captiondiv">Interior<br>or<br>Architect</div></li>
            	<li class='new4 image'><a href="#"><img src="img/thumb-4.jpg" alt=""></a></li>
            	<li class='new5 image'><a href="#"><img src="img/thumb-5.jpg" alt=""></a></li>
            	<li class='new5 caption'><div class="captiondiv">Interior<br>or<br>Architect</div></li>
            </ul>
        </div>
    </div>
    <div id="nav">
    	<ul>
        	<li class="active"><a href="#">Main Page</a></li>
        	<li><a href="#">About Us</a></li>
        	<li><a href="#">Projects</a></li>
        	<li><a href="#">Contact Us</a></li>
        </ul>
    </div>
	<div class="footer">
    	<div class="left">&copy; M R Fegde &amp; Associates 2011</div>
        <div class="right">
        	<ul class="social-ico">
            	<li><a href="#"><img src="img/mail-ico.png" alt=""></a></li>
            	<li><a href="#"><img src="img/fb-ico.png" alt=""></a></li>
            	<li><a href="#"><img src="img/tw-ico.png" alt=""></a></li>
        	</ul>
        </div>
    </div>
</div>
</body>
</html>