<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Highland</title>
<link href="css/style.css" type="text/css" rel="stylesheet" media="screen" />
<link href="css/contact.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="wrap">
<!--Start Wrap-->
<header>
  <div class="logo">logo</div>
  <div class="menu">
    <ul>
      <li style="background:#2e2e2e; padding:10px 10px; border:1px solid #000""><a style="background:#161616; padding:9px 9px; border:1px solid #666"" href="contactus.php">Contact us</a></li>
      <li class="selected" style="background:#2e2e2e; padding:10px 10px; border:1px solid #000""><a style="background:#161616; padding:9px 9px; border:1px solid #666"" href="studies.php">Case Studies</a></li>
      <li style="background:#2e2e2e; padding:10px 10px; border:1px solid #000""><a style="background:#161616; padding:9px 9px; border:1px solid #666" href="index.php">What we do</a></li>
    </ul>
  </div>
</header>
<div class="content">
  <div class="right">
    <h1><span style="color:#fff">Features</span></h1>
    <p><span style="color:#bef3ff;">There are many variations of passages</span><br />
    </p>
    <p style="padding:5px 0px;">You are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. </p>
    <br/>
    <br/>
    <p><span style="color:#bef3ff;">Internet tend to repeat predefined chunks</span></p>
    <p style="padding:5px 0px;">Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc. </p>
  </div>
  <!-- right content end-->
  <div class="main-content">
    <h1><span style="color:#bef3ff">Case</span> Studies</h1>
    <p><img id="dailog2" class="dailog" src="#" />Lorem Ipsum is <span style="color:#FFF;">simply dummy text</span> of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1000s,  scrambled it to make a type specimen book. </p>
    <div class="sarfaraz">
      <h2><span style="color:#bef3ff">Tasty</span> stuff</h2>
      <p class="p">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1000s,  scrambled it to make a type specimen book. </p>
      <div class="khan">
        <h2><span style="color:#bef3ff">Biggest</span> hits</h2>
        <p class="p">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
      </div>
      <div class="para">
        <p>Lorem Ipsum is <span style="color:#FFF;">simply dummy text </span>of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1000s,  scrambled it to make a type specimen book.</p>
        <p class="para2">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
      </div>
    </div>
    <!--End content-->
  </div>
  <!--End main content-->
</div>
<!--End Wrap-->
<footer>
  <div class="left">&copy;<a href="www.sarfarazalam.com">www.sarfarazalam.com</a>&nbsp;2011</div>
  <div class="right">Design and Developed by<a href="www.sarfarazalam.com">&nbsp;KH@N</div>
</footer>
</body>
</html>
